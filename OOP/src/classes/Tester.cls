/**
 * Created by User on 23.01.2019.
 */

public with sharing class Tester {
    
	public Integer size = 0;
	public static void methodB(){}
	


    public Integer size = 0;
	public static void methodA(){}

    public  List<Account> a {
        get{
            return a;
        }
        set{
            size = a.size();
        }
    }
	
    private List<Account> getAccounts(){
        List<Account> accs = new List<Account>();
        for(Account x: [SELECT Id, Activity__c, (SELECT Id FROM Contacts) FROM Account]){
            if(x.Contacts.size() == 0 ){
                accs.add(x);
            }
        }
        return accs;
    }
    public Tester(){
        a = new List<Account>();
    }
}