/**
 * Created by User on 16.01.2019.
 */

public with sharing class PublicContact extends Type{
    private Integer volunteerNumber;
    public Integer getVolunteerNumber() {
        return volunteerNumber;
    }
    public PublicContact(Contact con){
        super(con, 'public');
        volunteerNumber = generateNumber();
        if(Math.mod(volunteerNumber, 2) == 1)
            volunteerNumber++;
    }
}