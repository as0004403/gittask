/**
 * Created by User on 16.01.2019.
 */

public virtual with sharing class Type {
    protected String contactType;
    protected Double amountOfMoney;
    private Contact contactRecord;
    protected String childType;
    public String getContactType (){
        return contactType;
    }
    public Type(Contact con,String childType){
        contactRecord = con;
        amountOfMoney = con.Cost__c;
        this.childType = childType;
        contactType = '';
        if(Math.random() > 0.5)
            contactType = 'Premier';
    }
    public virtual Contact getContactRecord() {
        return contactRecord;
    }
    public static Integer generateNumber(){
        String rnd = String.valueOf(Math.random());
        for(Integer i = 2; i<rnd.length(); i++){
            if(rnd.substring(i,i+1) != '0') {
                rnd = rnd.substring(i, i + 6);
                break;
            }
        }
        return Integer.valueOf(rnd);
    }

    public String getChildType(){
        return childType;
    }
}