public with sharing class AccountSwitherHerImpl implements AccountSwither{

    public static List<Type> allContacts = new List<Type>();

    public static void getAndSort(){
        for(Contact con: [SELECT Cost__c, AccountId FROM Contact]) {
            if(con.cost__c > 10000) {
                allContacts.add(new PrivateContact(con));
            }
            else {
                allContacts.add(new PublicContact(con));
            }
        }

    }

    public static DataResult switchAccount() {
        return new DataResult().switchAcc();
    }

    public class DataResult {
        public List<PrivateContact> ChangedPrivateContacts;
        public List<PublicContact> ChangedPublicContacts;
        public List<PrivateContact> NonChangedPrivateContacts;
        public DataResult() {
            ChangedPrivateContacts = new List<PrivateContact>();
            ChangedPublicContacts = new List<PublicContact>();
            NonChangedPrivateContacts = new List<PrivateContact>();
        }

        DataResult switchAcc() {
            List<PrivateContact> prCons = new List<PrivateContact>();//List for all private contacts of allContacts
            List<PublicContact> pbCons = new List<PublicContact>();//List for all public contacts of allContacts
            for (Type cont : allContacts) {
                if (cont instanceof PrivateContact) {
                    prCons.add((PrivateContact)cont);
                    continue;
                }
                pbCons.add((PublicContact)cont);
            }
            Integer index = 0;
            Integer indexOfLastChanged = null;
            for (PrivateContact prCon : prCons) {
                if (index == pbCons.size()) {
                    indexOfLastChanged = ChangedPrivateContacts.indexOf(prCon);
                    break;
                }
                if (prCon.getContactType() == 'Premier') {
                    NonChangedPrivateContacts.add(prCon);
                    continue;
                }
                changeAccounts(prCon, pbCons[index]);
                index++;
            }
            if(indexOfLastChanged != null) {
                for (Integer i = indexOfLastChanged; i < prCons.size(); i++) {
                    NonChangedPrivateContacts.add(prCons[i]);
                }
            }
            updateChangedContacts();
            return this;
        }

        private void changeAccounts(PrivateContact prCon, PublicContact pbCon){
            Id privateId = prCon.getContactRecord().AccountId;
            Id publicId = pbCon.getContactRecord().AccountId;
            prCon.getContactRecord().AccountId = publicId;
            pbCon.getContactRecord().AccountId = privateId;
            ChangedPrivateContacts.add(prCon);
            ChangedPublicContacts.add(pbCon);
        }

        private void updateChangedContacts(){
            List<Contact> contactsForUpdate = new List<Contact>();
            for(PrivateContact prCon: ChangedPrivateContacts) {
                contactsForUpdate.add(prCon.getContactRecord());
            }
            for(PublicContact pbCon: ChangedPublicContacts){
                contactsForUpdate.add(pbCon.getContactRecord());
            }
            try {
                update contactsForUpdate;
            }
            catch (DmlException e){
                System.debug(e.getMessage());
                System.debug(e.getLineNumber());
            }
        }
    }
}