@isTest

public class CrowCalcTest {
    @testSetup
    public static void create(){
        createRecords(5000);
    }
    @isTest
    public static void testAddCrows() {
        Integer initialCrows = TotalSize();
        Boolean addCrowsResult = CrowCalc.addCrows(10);
        Integer crowsAfter = TotalSize() - initialCrows;
        System.assert(crowsAfter == 10, 'Wrong answer!');
        System.assert(addCrowsResult, 'Success addition not "true"');
    }
    @isTest
    public static void testSubtractCrows() {
        Integer rows=TotalSize();
        Boolean subtractCrowsResult = CrowCalc.subtractCrows(5);
        System.assert(rows-5==TotalSize());
        System.assert(subtractCrowsResult);
    }
    @isTest
    public static void testGetTotal() {
        System.assert(CrowCalc.getTotal()==TotalSize());
    }
    @isTest
    public static void testResetCalc() {
        Boolean resetCalcResult=CrowCalc.resetCalc();
        Integer rows=TotalSize();
        System.assert(rows==0);
        System.assert(resetCalcResult);
    }
    @isTest
    public static void negativeTestAddRows(){
        Boolean  addCrowsResult=CrowCalc.addCrows(5001);
        System.assert(!addCrowsResult);
    }
    @isTest
    public static void negativeTestSubtractCrows(){
        Boolean subtractCrowsResult=CrowCalc.subtractCrows(-1);
        System.assert(!subtractCrowsResult);
    }

    private static void createRecords(Integer numb){
        CrowCalc.addCrows(numb);
    }
    private static Integer TotalSize(){
        return [SELECT COUNT() FROM Crow__c];
    }

}