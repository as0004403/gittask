public with sharing class CrowCalc {

    // add new crows to DB, retutn true on success

    public static Boolean addCrows(Integer numberOfCrows) {

        if (numberOfCrows == null||numberOfCrows<0||numberOfCrows+getTotal()>9999) {
            return false;
        }

        List<Crow__c> crowsToAdd = new List<Crow__c>();
        for (Integer i = 0; i < numberOfCrows; i++) {
            crowsToAdd.add(new Crow__c());
        }
        try {
            insert crowsToAdd;
            return true;
        } catch (Exception e) {
            System.debug(e.getMessage());
            return false;
        }
    }



    // subtract crows from DB, retutn true on success

    public static Boolean subtractCrows(Integer numberOfCrows) {
        if (numberOfCrows == null||numberOfCrows<0) {
            return false;
        }
        List<Crow__c> crowsToSubtract = [SELECT Id FROM Crow__c LIMIT :numberOfCrows];
        try {
            delete crowsToSubtract;
        } catch (Exception e) {
            System.debug(e.getMessage());
            return false;
        }
        return true;
    }
    // return number of crows in DB
    public static Integer getTotal() {
        return [SELECT COUNT() FROM Crow__c];
    }
    // delete all crows
    public static Boolean resetCalc() {
        return subtractCrows(getTotal());

    }

}