/**
 * Created by User on 16.01.2019.
 */

public with sharing class PrivateContact extends Type{
    private Integer cardNumber;
    public PrivateContact(Contact con){
        super(con, 'private');
        cardNumber = generateNumber();
    }
    public Integer getCardNumber(){
        return cardNumber;
    }
}