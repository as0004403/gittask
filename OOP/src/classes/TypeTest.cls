/**
 * Created by User on 17.01.2019.
 */

@IsTest
private class TypeTest {
    @testSetup
    static void createRecords(){
        List<Contact> newContacts = new List<Contact>();
        for(Integer i = 0; i < 50; i++) {
            newContacts.add(new Contact(LastName = 'test' + i, Cost__c = 10001));
            newContacts.add(new Contact(LastName = 'test' + i, Cost__c = 9999));
        }
        insert newContacts;
    }
    static testMethod void testTypeClass() {
        Contact con = [SELECT Cost__c FROM Contact LIMIT 1];
        Type newType = new Type(con, 'private');
        System.assert(newType.getContactRecord() == con);
    }

    static testMethod void testGenerateNumber(){
        Contact con = [SELECT Cost__c FROM Contact LIMIT 1];
        Type newType = new Type(con, 'private');
        System.assert(Type.generateNumber() > 99999 && Type.generateNumber() < 1000000);
    }
    static testMethod void testCreatePublicContact(){
        Contact con = [SELECT Cost__c FROM Contact LIMIT 1];
        PublicContact pbCon = new PublicContact(con);
        System.assert(pbCon.getChildType() == 'public');
        System.assert(Math.mod(pbCon.getVolunteerNumber(), 2) == 0);
    }
    static testMethod void testCreatePrivateContact() {
        Contact con = [SELECT Cost__c FROM Contact LIMIT 1];
        PrivateContact prCon = new PrivateContact(con);
        System.assert(prCon.getChildType() == 'private');
    }
    static testMethod void testGetAndSort() {
        AccountSwitherHerImpl.getAndSort();
        Integer numOfPrivate = 0;
        System.assert(AccountSwitherHerImpl.allContacts.size() == 100);
    }
    static testMethod void testNumberOfPrivate() {
        AccountSwitherHerImpl.getAndSort();
        System.assert(getNumberOfPrivateOrPublic('private') == 50);
    }
    static testMethod void testNumberOfPublic() {
        AccountSwitherHerImpl.getAndSort();
        System.assert(getNumberOfPrivateOrPublic('public') == 50);
    }

    static testMethod void testNumberOfChangedContacts() {
        AccountSwitherHerImpl.getAndSort();
        Integer numberOfNonPremier = getNumberOfPrivateOrPublic('private') - getNumberOfPremier();
        Integer numberOfChanged = (numberOfNonPremier < getNumberOfPrivateOrPublic('public'))
                ? numberOfNonPremier : getNumberOfPrivateOrPublic('public');
        AccountSwitherHerImpl.DataResult result = AccountSwitherHerImpl.switchAccount();
        System.assert(result.ChangedPrivateContacts.size() == numberOfChanged);
        System.assert(result.ChangedPublicContacts.size() == numberOfChanged);
        System.assert(result.NonChangedPrivateContacts.size() == getNumberOfPrivateOrPublic('private') - result.ChangedPrivateContacts.size());
    }

    static Integer getNumberOfPrivateOrPublic(String childType) {
        Integer numOfPrivateOrPublic = 0;
        for(Type t : AccountSwitherHerImpl.allContacts){
            if(t.getChildType() != childType) continue;
            numOfPrivateOrPublic++;
        }
        return numOfPrivateOrPublic;
    }
    static Integer getNumberOfPremier(){
        Integer numOfPremier = 0;
        for(Type t: AccountSwitherHerImpl.allContacts) {
            if(t.getChildType() == 'private' && t.getContactType() == 'Premier') {
                numOfPremier++;
            }
        }
        return numOfPremier;
    }
}