

public with sharing class Contact1TriggerHandler {
    public static List<Account> accs;

    public static void setIsAccountSet(List<Contact> contacts) {
        setAvailableAccounts();
        List<DateDistance> newDateDistances = new List<DateDistance>();
        for(Account a: accs) {
            newDateDistances.add(new DateDistance(a));
        }
        sortList(newDateDistances);
        Integer size = (contacts.size() < newDateDistances.size()) ? contacts.size() : newDateDistances.size();
        for(Integer contNumber = 0; contNumber < size; contNumber++) {
            contacts[contNumber].AccountId = newDateDistances[contNumber].acc.Id;
            contacts[contNumber].IsAccountSet__c = true;
        }
    }

    private static void setAvailableAccounts() {
        List<Id> notAvailableAccountsIds = new List<Id>();
        for(Contact c: [SELECT AccountId FROM Contact WHERE IsAccountSet__c=true AND AccountId<>null]) {
            notAvailableAccountsIds.add(c.AccountId);
        }
        accs = new List<Account>();
        accs = [SELECT Id, Activity__c FROM Account WHERE NOT (Id IN :notAvailableAccountsIds)];
    }
    private static Date getAverageDate(){
        Integer averageNumberOfDays = 0;
        Integer numberOfDates = 0;
        for(Account a: accs){
            if(a.Activity__c != null) {
                averageNumberOfDays += Date.today().daysBetween(a.Activity__c);
                numberOfDates++;
            }
        }
        Date averageDate = Date.today();
        if(numberOfDates != 0) {
           averageDate = Date.today().addDays((Integer)(averageNumberOfDays / numberOfDates));
        }
        return averageDate;
    }

    private static void sortList(List<DateDistance> accountsWithDifferences) {
        List<DateDistance> notSortElements = new List<DateDistance>();
        for(Integer i = 0; i < accountsWithDifferences.size(); i++) {
            if(accountsWithDifferences[i].differenceBetweenAverage == null) {
                notSortElements.add(new DateDistance(accountsWithDifferences[i].acc));
                accountsWithDifferences.remove(i);
                i--;
            }
        }
        Boolean b = true;
        while (b){
            b = false;
            for (Integer i = 0; i < accountsWithDifferences.size() - 1; i++) {
                if (accountsWithDifferences[i].differenceBetweenAverage >
                        accountsWithDifferences[i + 1].differenceBetweenAverage) {
                    swap(accountsWithDifferences[i], accountsWithDifferences[i + 1]);
                    b = true;
                }
            }
        }
        accountsWithDifferences.addAll(notSortElements);
    }
    private static void swap(DateDistance d1, DateDistance d2) {
        Account acc1 = d1.acc;
        Integer diff1 = d1.differenceBetweenAverage;
        d1.acc = d2.acc;
        d1.differenceBetweenAverage = d2.differenceBetweenAverage;
        d2.acc = acc1;
        d2.differenceBetweenAverage = diff1;
    }

    public class DateDistance {
        public Account acc;
        public Integer differenceBetweenAverage;
        public DateDistance(Account acc) {
            this.acc = acc;
            if(acc.Activity__c != null) {
                differenceBetweenAverage = Math.abs(getAverageDate().daysBetween(acc.Activity__c));
            }
        }
    }

}